package com.hcl.librarymanagement.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Builder;

@Builder
public record BorrowBookDto(Long bookId,
		@Min(value = 1, message = "Minimum value must be 1") @Max(value = 1, message = "Maximum value must be 1") Integer noOfCopies) {
}
