package com.hcl.librarymanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class ReturnRequestDto {
	private Long userId;
	private Long bookBorrowId;
}
