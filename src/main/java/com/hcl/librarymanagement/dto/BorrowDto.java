package com.hcl.librarymanagement.dto;

import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import lombok.Builder;

@Builder
public record BorrowDto(Long userId,
		@Size(min = 1, max = 3, message = "Only 3 Books Borrowed at once") @Valid List<BorrowBookDto> borrowBookDtos) {

}
