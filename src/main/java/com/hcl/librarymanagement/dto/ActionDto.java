package com.hcl.librarymanagement.dto;

import com.hcl.librarymanagement.entity.Status;

import lombok.Builder;

@Builder
public record ActionDto(Long borrowBookId, Status action) {

}
