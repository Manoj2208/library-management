package com.hcl.librarymanagement.dto;

import java.util.List;

import lombok.Builder;

@Builder
public record BorrowApprovalDto(Long userId, List<ActionDto> actionDtos) {
	
}
