package com.hcl.librarymanagement.exception;

public class BookBorrowNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BookBorrowNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public BookBorrowNotFound() {
		super("Resource Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
