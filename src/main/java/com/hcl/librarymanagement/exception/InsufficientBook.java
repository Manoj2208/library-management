package com.hcl.librarymanagement.exception;

public class InsufficientBook extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientBook(String message) {
		super(message, GlobalErrorCode.ERROR_BAD_REQUEST);
	}

	public InsufficientBook() {
		super("Unauthorized Customer", GlobalErrorCode.ERROR_BAD_REQUEST);
	}
}
