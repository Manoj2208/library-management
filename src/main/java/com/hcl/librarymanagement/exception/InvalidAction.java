package com.hcl.librarymanagement.exception;

public class InvalidAction extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidAction(String message) {
		super(message, GlobalErrorCode.ERROR_BAD_REQUEST);
	}

	public InvalidAction() {
		super("Invalid Action", GlobalErrorCode.ERROR_BAD_REQUEST);
	}
}
