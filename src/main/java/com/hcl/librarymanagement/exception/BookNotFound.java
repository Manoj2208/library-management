package com.hcl.librarymanagement.exception;

public class BookNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BookNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public BookNotFound() {
		super("Book Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
