package com.hcl.librarymanagement.exception;

public class Unauthorized extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Unauthorized(String message) {
		super(message, GlobalErrorCode.ERROR_UNAUTHOURIZED);
	}

	public Unauthorized() {
		super("Unauthorized", GlobalErrorCode.ERROR_UNAUTHOURIZED);
	}
}
