package com.hcl.librarymanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.librarymanagement.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
