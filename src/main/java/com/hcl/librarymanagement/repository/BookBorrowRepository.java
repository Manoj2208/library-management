package com.hcl.librarymanagement.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.librarymanagement.entity.BookBorrow;
import com.hcl.librarymanagement.entity.User;

public interface BookBorrowRepository extends JpaRepository<BookBorrow, Long> {

	Optional<BookBorrow> findByBookBorrowIdAndUser(Long bookBorrowId, User user);

	

}
