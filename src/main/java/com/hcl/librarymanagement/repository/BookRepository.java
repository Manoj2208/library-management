package com.hcl.librarymanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.librarymanagement.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

	

	List<Book> findByBookNameContainingIgnoreCaseAndCategoryContainingIgnoreCase(String bookName, String category);

	List<Book> findByBookNameContainingIgnoreCase(String bookName);

	List<Book> findByCategoryContainingIgnoreCase(String category);

	List<Book> findByAuthorNameContainingIgnoreCase(String authorName);

}
