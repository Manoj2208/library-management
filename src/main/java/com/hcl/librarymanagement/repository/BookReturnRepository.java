package com.hcl.librarymanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.librarymanagement.entity.BookReturn;

public interface BookReturnRepository extends JpaRepository<BookReturn, Long> {

}
