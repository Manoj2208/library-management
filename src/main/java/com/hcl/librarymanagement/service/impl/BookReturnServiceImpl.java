package com.hcl.librarymanagement.service.impl;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.springframework.stereotype.Service;

import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.ReturnRequestDto;
import com.hcl.librarymanagement.entity.Book;
import com.hcl.librarymanagement.entity.BookBorrow;
import com.hcl.librarymanagement.entity.BookReturn;
import com.hcl.librarymanagement.entity.Status;
import com.hcl.librarymanagement.entity.User;
import com.hcl.librarymanagement.exception.BorrowIdNotFoundException;
import com.hcl.librarymanagement.exception.ResourceConflictException;
import com.hcl.librarymanagement.exception.Unauthorized;
import com.hcl.librarymanagement.exception.UserNotFound;
import com.hcl.librarymanagement.repository.BookBorrowRepository;
import com.hcl.librarymanagement.repository.BookRepository;
import com.hcl.librarymanagement.repository.BookReturnRepository;
import com.hcl.librarymanagement.repository.UserRepository;
import com.hcl.librarymanagement.service.BookReturnService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class BookReturnServiceImpl implements BookReturnService {

	private final BookBorrowRepository bookBorrowRepository;
    private final BookRepository bookRepository;
    private final UserRepository userRepository;
    private final BookReturnRepository bookReturnRepository;
 
    @Override
    public ApiResponse returnBook(ReturnRequestDto returnRequestDto) {
		User user=userRepository.findById(returnRequestDto.getUserId()).orElseThrow(UserNotFound::new);
		BookBorrow bookBorrow =bookBorrowRepository.findById(returnRequestDto.getBookBorrowId()).orElseThrow(BorrowIdNotFoundException::new);
		if(bookBorrowRepository.findByBookBorrowIdAndUser(bookBorrow.getBookBorrowId(), user).isEmpty())
			throw new ResourceConflictException();
		
		if(bookBorrow.getStatus().equals(Status.Approve)) {
			double charge=0;
			LocalDate dueDate = bookBorrow.getDueDate();
            LocalDate returnDate = LocalDate.now();
            long daysOverdue = ChronoUnit.DAYS.between(dueDate, returnDate);
            if (daysOverdue > 0) {
                charge = daysOverdue <= 3 ? daysOverdue * 20 : (daysOverdue - 3) * 50 + 60;
                // Charge overdue fee
            }
			BookReturn bookReturn=new BookReturn();
			bookReturn.setPenalty(charge);
			bookReturn.setReturnDate(LocalDate.now());
			bookReturn.setOverDue(true);
			bookReturn.setBookBorrow(bookBorrow);
			bookBorrow.setStatus(Status.Returned);
            Book book = bookBorrow.getBook();
            book.setNoOfCopies(book.getNoOfCopies()+bookBorrow.getNoOfCopies());
            bookRepository.save(book);
			bookBorrowRepository.save(bookBorrow);
			bookReturnRepository.save(bookReturn);
			return new ApiResponse("Book Returned Successfully", 201L);		
		}
		
		throw new Unauthorized("Verify borrowId");
    }



}
