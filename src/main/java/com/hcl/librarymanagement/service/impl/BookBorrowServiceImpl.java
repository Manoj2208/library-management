package com.hcl.librarymanagement.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.BorrowApprovalDto;
import com.hcl.librarymanagement.dto.BorrowDto;
import com.hcl.librarymanagement.entity.Book;
import com.hcl.librarymanagement.entity.BookBorrow;
import com.hcl.librarymanagement.entity.Role;
import com.hcl.librarymanagement.entity.Status;
import com.hcl.librarymanagement.entity.User;
import com.hcl.librarymanagement.exception.BookBorrowNotFound;
import com.hcl.librarymanagement.exception.BookNotFound;
import com.hcl.librarymanagement.exception.InsufficientBook;
import com.hcl.librarymanagement.exception.InvalidAction;
import com.hcl.librarymanagement.exception.Unauthorized;
import com.hcl.librarymanagement.exception.UserNotFound;
import com.hcl.librarymanagement.repository.BookBorrowRepository;
import com.hcl.librarymanagement.repository.BookRepository;
import com.hcl.librarymanagement.repository.UserRepository;
import com.hcl.librarymanagement.service.BookBorrowService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookBorrowServiceImpl implements BookBorrowService {

	private final UserRepository userRepository;
	private final BookRepository bookRepository;
	private final BookBorrowRepository bookBorrowRepository;

	@Override
	@Transactional
	public ApiResponse borrow(BorrowDto borrowDto) {
		User user = userRepository.findById(borrowDto.userId()).orElseThrow(() -> {
			log.error("Invalid userId caused to ResourceNotFound");
			throw new UserNotFound("User Not Found");
		});

		List<BookBorrow> bookBorrows = new ArrayList<>();
		borrowDto.borrowBookDtos().forEach(borrowBookDto -> {
			Book book = bookRepository.findById(borrowBookDto.bookId()).orElseThrow(() -> {
				throw new BookNotFound();
			});
			if (book.getNoOfCopies() > borrowBookDto.noOfCopies()) {
				book.setNoOfCopies(book.getNoOfCopies() - borrowBookDto.noOfCopies());
				BookBorrow bookBorrow = BookBorrow.builder().book(book).borrowDate(LocalDate.now())
						.dueDate(LocalDate.now().plusWeeks(2)).status(Status.PendingApproval).user(user)
						.noOfCopies(borrowBookDto.noOfCopies()).build();
				bookBorrows.add(bookBorrow);
				bookRepository.save(book);
			} else
				throw new InsufficientBook("requesting book copies not available ");

		});
		bookBorrowRepository.saveAll(bookBorrows);
		return ApiResponse.builder().message("Book Borrowed successfully").httpStatus(201l).build();
	}

	@Override
	@Transactional
	public ApiResponse approveBorrowals(BorrowApprovalDto borrowApprovalDto) {
		User user = userRepository.findById(borrowApprovalDto.userId()).orElseThrow(() -> {
			log.error("Invalid userId caused to ResourceNotFound");
			throw new UserNotFound("Librarian Not Found");
		});
		if (!(user.getRole().equals(Role.Librarian)))
			throw new Unauthorized();

		borrowApprovalDto.actionDtos().forEach(actionDto -> {
			BookBorrow bookBorrow = bookBorrowRepository.findById(actionDto.borrowBookId()).orElseThrow(() -> {
				throw new BookBorrowNotFound("Invalid bookBorrowId");
			});
			if ((actionDto.action().equals(Status.Approve)) && (bookBorrow.getStatus().equals(Status.PendingApproval)))
				bookBorrow.setStatus(Status.Approve);
			else if (bookBorrow.getStatus().equals(Status.PendingApproval)&&(actionDto.action().equals(Status.Reject))) {
				bookBorrow.setStatus(Status.Reject);
				Book book = bookBorrow.getBook();
				book.setNoOfCopies(book.getNoOfCopies() + bookBorrow.getNoOfCopies());
				bookRepository.save(book);
			} else
				throw new InvalidAction();
			bookBorrowRepository.save(bookBorrow);
		});
		return ApiResponse.builder().message("Librarian Responded on BorrowRequest").httpStatus(200l).build();

	}
}
