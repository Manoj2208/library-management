package com.hcl.librarymanagement.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.librarymanagement.entity.Book;
import com.hcl.librarymanagement.repository.BookRepository;
import com.hcl.librarymanagement.service.BookSearchService;

import lombok.RequiredArgsConstructor;
@RequiredArgsConstructor
@Service
public class BookSearchServiceImpl implements BookSearchService {
	private final BookRepository bookRepository;
	
	public List<Book> searchbooks(String bookName, String category, String authorName) {
		if(bookName != null && category != null) {
			return bookRepository.findByBookNameContainingIgnoreCaseAndCategoryContainingIgnoreCase( bookName, category );
			
		}else if (bookName != null) {
			return bookRepository.findByBookNameContainingIgnoreCase(bookName);
		}else if (category != null) {
			return bookRepository.findByCategoryContainingIgnoreCase(category);
			
		}else if (authorName != null) {
			return bookRepository.findByAuthorNameContainingIgnoreCase(authorName);
		}
		else {
			return bookRepository.findAll();
		}
	
	}
	

}
