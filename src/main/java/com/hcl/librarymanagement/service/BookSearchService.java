package com.hcl.librarymanagement.service;

import java.util.List;

import com.hcl.librarymanagement.entity.Book;

public interface BookSearchService {

	List<Book> searchbooks(String bookName, String category, String authorName);

	
}
