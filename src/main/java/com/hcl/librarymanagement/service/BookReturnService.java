package com.hcl.librarymanagement.service;

import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.ReturnRequestDto;

public interface BookReturnService {

	ApiResponse returnBook(ReturnRequestDto returnRequest);

}
