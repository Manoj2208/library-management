package com.hcl.librarymanagement.service;

import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.BorrowApprovalDto;
import com.hcl.librarymanagement.dto.BorrowDto;

public interface BookBorrowService {

	ApiResponse borrow(BorrowDto borrowDto);

	ApiResponse approveBorrowals(BorrowApprovalDto borrowApprovalDto);

}
