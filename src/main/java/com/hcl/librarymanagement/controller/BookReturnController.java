package com.hcl.librarymanagement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.ReturnRequestDto;
import com.hcl.librarymanagement.service.BookReturnService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class BookReturnController {

	private final BookReturnService bookReturnService;

	@PostMapping("/book_return")
	public ResponseEntity<ApiResponse> returnBooks(@RequestBody ReturnRequestDto returnRequestDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(bookReturnService.returnBook(returnRequestDto));
		
		
	}
}
