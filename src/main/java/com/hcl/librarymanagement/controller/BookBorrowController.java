package com.hcl.librarymanagement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.BorrowDto;
import com.hcl.librarymanagement.service.BookBorrowService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/borrow-books")
@RequiredArgsConstructor
public class BookBorrowController {
	private final BookBorrowService bookBorrowService;

	@PostMapping
	public ResponseEntity<ApiResponse> borrowBook(@RequestBody @Valid BorrowDto borrowDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(bookBorrowService.borrow(borrowDto));
	}
}
