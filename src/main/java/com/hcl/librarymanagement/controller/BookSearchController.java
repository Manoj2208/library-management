package com.hcl.librarymanagement.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.librarymanagement.entity.Book;
import com.hcl.librarymanagement.service.BookSearchService;

import lombok.RequiredArgsConstructor;
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/books")
public class BookSearchController {
	private final BookSearchService bookSearchService;
	@GetMapping
	public ResponseEntity<List<Book>> searchbooks(@RequestParam(required = false) String bookName,
			@RequestParam(required = false) String category, @RequestParam(required = false) String authorName) {
		return new ResponseEntity<>(bookSearchService.searchbooks(bookName,category,authorName), HttpStatus.OK);
	}
	
 
}
