package com.hcl.librarymanagement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.BorrowApprovalDto;
import com.hcl.librarymanagement.service.BookBorrowService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/borrow-books")
@RequiredArgsConstructor
public class LibrarianActionController {
	private final BookBorrowService bookBorrowService;

	@PutMapping
	public ResponseEntity<ApiResponse> approveBorrowals(@RequestBody BorrowApprovalDto borrowApprovalDto) {
		return ResponseEntity.status(HttpStatus.OK).body(bookBorrowService.approveBorrowals(borrowApprovalDto));
	}
}
