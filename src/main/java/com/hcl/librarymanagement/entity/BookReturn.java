package com.hcl.librarymanagement.entity;

import java.time.LocalDate;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BookReturn {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bookReturnId;
	@OneToOne
	private BookBorrow bookBorrow;
	@CreationTimestamp
	private LocalDate returnDate;
	private boolean isOverDue;
	private Double penalty;
}
