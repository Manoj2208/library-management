package com.hcl.librarymanagement.entity;

public enum Status {
	PendingApproval,Approve,Reject,Returned
}
