package com.hcl.librarymanagement.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookBorrow {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bookBorrowId;
	@ManyToOne
	private Book book;
	@ManyToOne
	private User user;
	@Enumerated(EnumType.STRING)
	private Status status;
	private Integer noOfCopies;
	private LocalDate borrowDate;
	private LocalDate dueDate;
	
}
