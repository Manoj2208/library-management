package com.hcl.librarymanagement;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.librarymanagement.entity.Book;
import com.hcl.librarymanagement.repository.BookRepository;
import com.hcl.librarymanagement.service.impl.BookSearchServiceImpl;

@ExtendWith(SpringExtension.class)
public class BookSearchServiceImplTest {
	@InjectMocks 
	private BookSearchServiceImpl bookSearchServiceImpl;
	@Mock
	private BookRepository bookRepository;
	
	@Test
	void searchByBookNameAndCategory() {
		List<Book> bookList = Arrays.asList(new Book(), new Book());
		Mockito.when(bookRepository.findByBookNameContainingIgnoreCaseAndCategoryContainingIgnoreCase(toString(),toString())).thenReturn(bookList);
		List<Book> seachBook = bookSearchServiceImpl.searchbooks("java", "programming",null);
		
	}
	
	@Test
	void searchByBookName() {
		List<Book> bookList = Arrays.asList(new Book(), new Book());
		Mockito.when(bookRepository.findByBookNameContainingIgnoreCase(toString())).thenReturn(bookList);
		List<Book> seachBook = bookSearchServiceImpl.searchbooks("java", null,null);
	}
	@Test
	void searchByAll() {
		List<Book> bookList = Arrays.asList(new Book(), new Book());
		Mockito.when(bookRepository.findAll()).thenReturn(bookList);
		List<Book> seachBook = bookSearchServiceImpl.searchbooks(null, null, null);
		
	}
	@Test
	void searchByCategory() {
		List<Book> bookList = Arrays.asList(new Book(), new Book());
		Mockito.when(bookRepository.findByCategoryContainingIgnoreCase(toString())).thenReturn(bookList);
		List<Book> seachBook = bookSearchServiceImpl.searchbooks(null, "programming",null);
		
	}
	@Test
	void searchByAuthorName() {
		List<Book> bookList = Arrays.asList(new Book(), new Book());
		Mockito.when(bookRepository.findByCategoryContainingIgnoreCase(toString())).thenReturn(bookList);
		List<Book> seachBook = bookSearchServiceImpl.searchbooks(null,null,"niharika");
	}
}
