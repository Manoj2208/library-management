package com.hcl.librarymanagement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.hcl.librarymanagement.dto.ReturnRequestDto;
import com.hcl.librarymanagement.entity.Book;
import com.hcl.librarymanagement.entity.BookBorrow;
import com.hcl.librarymanagement.entity.Status;
import com.hcl.librarymanagement.entity.User;
import com.hcl.librarymanagement.exception.Unauthorized;
import com.hcl.librarymanagement.repository.BookBorrowRepository;
import com.hcl.librarymanagement.repository.BookRepository;
import com.hcl.librarymanagement.repository.BookReturnRepository;
import com.hcl.librarymanagement.repository.UserRepository;
import com.hcl.librarymanagement.service.impl.BookReturnServiceImpl;

public class BookReturnServiceImplTest {
    @Mock
    private BookBorrowRepository bookBorrowRepository;
 
    @Mock
    private BookRepository bookRepository;
 
    @Mock
    private UserRepository userRepository;
 
    @Mock
    private BookReturnRepository bookReturnRepository;
 
    @InjectMocks
    private BookReturnServiceImpl bookReturnService;
 
    @Test
    public void returnBook_UnauthorizedUser() {
        // Given
        ReturnRequestDto requestDto = new ReturnRequestDto(1L, 1L);
        User user = new User();
        user.setUserId(1l);
        Book book=new  Book();
        
        BookBorrow bookBorrow = new BookBorrow();
        bookBorrow.setUser(user);
        bookBorrow.setBook(book);
        bookBorrow.setStatus(Status.PENDINGAPPROVAL);
        
        bookBorrow.setStatus(Status.APPROVE);
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(bookBorrowRepository.findById(1L)).thenReturn(Optional.of(bookBorrow));
        when(bookBorrowRepository.findByBookBorrowIdAndUser(1L, user)).thenReturn(Optional.of(bookBorrow));
 
        // When and Then
        Unauthorized exception = assertThrows(Unauthorized.class, () -> bookReturnService.returnBook(requestDto));
        assertEquals("Verify borrowId", exception.getMessage());
    }
}
