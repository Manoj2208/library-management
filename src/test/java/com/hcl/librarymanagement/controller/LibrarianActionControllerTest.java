package com.hcl.librarymanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.librarymanagement.dto.ActionDto;
import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.BorrowApprovalDto;
import com.hcl.librarymanagement.entity.Status;
import com.hcl.librarymanagement.service.BookBorrowService;

@ExtendWith(SpringExtension.class)
class LibrarianActionControllerTest {
	@Mock
	private BookBorrowService bookBorrowService;
	@InjectMocks
	private LibrarianActionController librarianActionController;

	@Test
	void testApproveBorrowals() {
		ActionDto actionDto = ActionDto.builder().action(Status.Approve).borrowBookId(1l).build();
		List<ActionDto> actionDtos = List.of(actionDto);
		BorrowApprovalDto borrowApprovalDto = BorrowApprovalDto.builder().userId(1l).actionDtos(actionDtos).build();
		ApiResponse apiResponse = ApiResponse.builder().message("Success").httpStatus(200l).build();
		Mockito.when(bookBorrowService.approveBorrowals(borrowApprovalDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=librarianActionController.approveBorrowals(borrowApprovalDto);
		assertNotNull(responseEntity);
		assertEquals("Success", responseEntity.getBody().message());
	}
}
