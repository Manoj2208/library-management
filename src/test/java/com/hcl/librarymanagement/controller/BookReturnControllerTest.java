package com.hcl.librarymanagement.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.ReturnRequestDto;
import com.hcl.librarymanagement.service.BookReturnService;

@ExtendWith(MockitoExtension.class)
public class BookReturnControllerTest {

	@Mock
	private BookReturnService bookReturnService;
	@InjectMocks
	private BookReturnController bookReturnController;

	@Test
	void Books_ReturnsCreatedResponse() {
		ReturnRequestDto requestDto = new ReturnRequestDto(1L, 1L);
		ApiResponse expectedResponse = new ApiResponse("Book Returned Successfully", 201L);
		when(bookReturnService.returnBook(any(ReturnRequestDto.class))).thenReturn(expectedResponse);
		ResponseEntity<ApiResponse> responseEntity = bookReturnController.returnBooks(requestDto);
		verify(bookReturnService).returnBook(requestDto);
        assert responseEntity.getStatusCode() == HttpStatus.CREATED;
        assert responseEntity.getBody() != null;
        assert responseEntity.getBody().message().equals(expectedResponse.message());
        assert responseEntity.getBody().httpStatus().equals(expectedResponse.httpStatus());
    }
	

}
