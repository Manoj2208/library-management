package com.hcl.librarymanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.librarymanagement.dto.ApiResponse;
import com.hcl.librarymanagement.dto.BorrowBookDto;
import com.hcl.librarymanagement.dto.BorrowDto;
import com.hcl.librarymanagement.service.BookBorrowService;

@ExtendWith(SpringExtension.class)
class BookBorrowControllerTest {
	@Mock
private BookBorrowService bookBorrowService;
	@InjectMocks
private BookBorrowController bookBorrowController;
	
	@Test
	void testBookBorrow() {
		BorrowBookDto borrowBookDto=BorrowBookDto.builder().bookId(1l).noOfCopies(1).build();
		List<BorrowBookDto> borrowBookDtos=List.of(borrowBookDto);
		BorrowDto borrowDto=BorrowDto.builder().userId(123l).borrowBookDtos(borrowBookDtos).build();
		ApiResponse apiResponse=ApiResponse.builder().message("sucess").httpStatus(201l).build();
		Mockito.when(bookBorrowService.borrow(borrowDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=bookBorrowController.borrowBook(borrowDto);
		assertNotNull(responseEntity);
		assertEquals("sucess", responseEntity.getBody().message());
		
	}
}
